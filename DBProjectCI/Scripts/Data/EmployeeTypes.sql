﻿
-- Merge des valeurs de la table EmployeeTypes

MERGE INTO [dbo].[EmployeeType] AS TARGET
USING (VALUES
  (1,'Full Time')
 ,(3,'Intern')
 ,(4,'Executive')
 ,(5,'Temporary')
 ,(6,'Consultants')
 ,(29,'Part Time')
) AS Source ([Id], [Type])
ON (Target.[Id] = Source.[Id])
WHEN MATCHED AND (Target.[Type] <> Source.[Type]) THEN
    UPDATE SET
    [Type] = Source.[Type]
WHEN NOT MATCHED BY TARGET THEN
    INSERT([Type])
    VALUES(Source.[Type])
WHEN NOT MATCHED BY SOURCE THEN
    DELETE;
