﻿
INSERT INTO Employee (FirstName, LastName, Code)
VALUES ('Martin', 'Thibault', 'MTT')

GO

UPDATE FROM Employee
SET FirstName = 'Jean'
WHERE Code = 'LRP'


GO

DROP TABLE Employee


  