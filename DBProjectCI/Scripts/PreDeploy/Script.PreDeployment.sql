DECLARE @Key VARCHAR(255)


-- ScriptFile: DBP-001_EmployeeName_Update.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

UPDATE Employee
SET [LastName] = ''Charon''
WHERE Id = 1;

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PreDeploy')

-- ScriptFile: DBP-003_RunOnce.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

UPDATE Employee
SET [LastName] = ''Lévesque''
WHERE Id = 2;

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PreDeploy')

-- ScriptFile: DBP-005.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

INSERT INTO Employee (FirstName, LastName, Code)
VALUES (''Martin'', ''Thibault'', ''MTT'')

'

EXEC sp_executesql '

UPDATE FROM Employee
SET FirstName = ''Jean''
WHERE Code = ''LRP''

'

EXEC sp_executesql '

DROP TABLE Employee

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PreDeploy')

-- ScriptFile: DBP-006_NoRunOnceInstruction.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

INSERT INTO Employee (FirstName, LastName) 
VALUES (''Éric'', ''Veilleux'');

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PreDeploy')

