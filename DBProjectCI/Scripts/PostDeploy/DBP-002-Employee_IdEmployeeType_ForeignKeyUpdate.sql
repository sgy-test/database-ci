﻿

DECLARE @Key VARCHAR(20) = 'DBP-002'

IF NOT EXISTS (SELECT * FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN

	UPDATE Employee
	SET [IdEmployeeType] = 1;

INSERT INTO dbo.DeploymentScript (Id) VALUES (@Key)

END
