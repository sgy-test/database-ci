DECLARE @Key VARCHAR(255)


-- ScriptFile: AddConstraintEmployee.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

-- Build Script

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT uk_employe UNIQUE (Id);

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PostDeploy')

-- ScriptFile: DBP-002-Employee_IdEmployeeType_ForeignKeyUpdate.sql -------------------------------------

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN


EXEC sp_executesql '

DECLARE @Key VARCHAR(20) = ''DBP-002''

IF NOT EXISTS (SELECT * FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN

	UPDATE Employee
	SET [IdEmployeeType] = 1;

INSERT INTO dbo.DeploymentScript (Id) VALUES (@Key)

END

'

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, 'PostDeploy')

-- ScriptFile: EmployeeTypes.sql -------------------------------------

EXEC sp_executesql '

-- Merge des valeurs de la table EmployeeTypes

MERGE INTO [dbo].[EmployeeType] AS TARGET
USING (VALUES
  (1,''Full Time'')
 ,(3,''Intern'')
 ,(4,''Executive'')
 ,(5,''Temporary'')
 ,(6,''Consultants'')
 ,(29,''Part Time'')
) AS Source ([Id], [Type])
ON (Target.[Id] = Source.[Id])
WHEN MATCHED AND (Target.[Type] <> Source.[Type]) THEN
    UPDATE SET
    [Type] = Source.[Type]
WHEN NOT MATCHED BY TARGET THEN
    INSERT([Type])
    VALUES(Source.[Type])
WHEN NOT MATCHED BY SOURCE THEN
    DELETE;

'
