﻿CREATE TABLE [dbo].[Logs]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Description] VARCHAR(MAX) NOT NULL
)
