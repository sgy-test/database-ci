﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [Code] NCHAR(10) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [Temporaire] BIT NULL 
)
