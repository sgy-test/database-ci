﻿
$preDeployFilename = "Script.PreDeployment.sql"


Clear-Host


#$scriptType = 1 (Pre), 2 (Post), 3 (Data)



Add-Type -TypeDefinition @"
   public enum ScriptType
   {
      Pre,
      Post,
      Data
   }
"@


$scriptType = [ScriptType]::Post
$scriptType.value__




$runOnceCheck = @"

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN

"@


$runOnceEnd =

@"

END

INSERT INTO dbo.DeploymentScript (Id) VALUES (@key)

-- == END OF SCRIPT =========================================

"@

$generatedScript = @'
DECLARE @Key VARCHAR(20)

'@


Get-ChildItem ".\PreDeploy" -Exclude *PreDeployment* |
Foreach-Object {

    #$_.FullName


    $key = [regex]::Match($_.FullName, '([A-Z0-9]{2,5}-\d+(-\d+)?)')

    if ($key.Success) {

        $key = $key.Value

        $fileContent = Get-Content $_.FullName

        $runOnce = [regex]::Match($fileContent, '--\s?RunOnce:\s?([YN])')

        $contentToAppend = ""

        if ($runOnce.Success) {

            $contentToAppend +=
@"

SET @Key = '$key'

$runOnceCheck

"@

        }

        #$script = (Get-Content $_.FullName) -join "`n"

        $script = (Get-Content $_.FullName)

        $script.


        $script = $script.Replace("'", "''");



        $contentToAppend +=
@"

EXEC sp_executesql
'
$script
'

"@

        if ($runOnce.Success) {

            #$script = Get-Content $_.

            $contentToAppend += $runOnceEnd
        }
    }



    $generatedScript += $contentToAppend

}


$generatedScript
















<#
        #$contentToAppend += Get-Content $_.FullName
        #$contentToAppend += Out-File -Encoding Ascii -append $_.FullName

    #$content = Get-Content $_.FullName

    # $runOnce = $content -match '--\s?RunOnce:\s?([YN])'

    #$key = [regex]::Match($content, '-- Key:\s?([A-Z0-9]{2,5}-\d+(-\d+)?)')
    #$runOnce = [regex]::Match($content, '--\s?RunOnce:\s?([YN])')

    #$_.FullName

    #$key = $key.Groups[1].Value
    #$runOnce = $runOnce.Groups[1].Value

    $contentToAppend = @"
-- Filename: $_
-- Key: $key

IF NOT EXISTS (SELECT * FROM dbo.DeploymentScript WHERE Id = '$key')
BEGIN

	UPDATE Employee
	SET [LastName] = 'Charon'
	WHERE Id = 1;

INSERT INTO dbo.DeploymentScript (Id) VALUES ('$key')

END

-- ======== EOF =================

"@

#>


# Set-Location ..\DBProjectCI\Scripts

#Get-ChildItem "..\DBProjectCI\Scripts"

# Remove-Item .\PreDeploy\Script.PreDeployment.sql -ErrorAction SilentlyContinue

#Clear-Content .\PreDeploy\$preDeployFilename


#$list = Get-ChildItem ".\PreDeploy" -Exclude *PreDeployment*

#$list

#$contentToAppend


