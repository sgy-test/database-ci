﻿
# function Remove-EmptyLines {
#     param(
#         [System.Collections.ArrayList] $script
#     )

#     $script | ForEach-Object {

#         $empty = $_.ToString().Trim().Equals("")

#         if (-Not $empty) {
#             $lastNotEmptyLine = $count
#         }

#         $count++
#     }

#     $script.RemoveRange($lastNotEmptyLine, $script.Count-$lastNotEmptyLine)

#     return $script
# }

# function Split-Script {

#     param (
#         [System.Collections.ArrayList] $script
#     )

#     $currentScript = New-Object System.Collections.ArrayList($null)
#     $scriptDivisions = New-Object System.Collections.ArrayList($null)

#     $script | ForEach-Object {

#         $currentLine = $_

#         $currentScript.Add($currentLine) > $null

#         $anyGO = [regex]::Match($currentLine, 'GO')
#         if ($anyGO.Success) {

#             $currentScript.RemoveAt($currentScript.Count-1)

#             $scriptDivisions.Add($currentScript) > $null
#             $currentScript.Clear()
#         }
#     }

#     return $scriptDivisions
# }

# function Split-Script {
#     Param (
#         [System.Collections.ArrayList] $script
#     )

#     $scriptPartLines = New-Object System.Collections.ArrayList($null)
#     $scriptParts = New-Object System.Collections.ArrayList($null)

#     ForEach ($line in $script) {
#         $scriptPartLines.Add($line) > $null

#         $anyGO = [regex]::Match($line, 'GO')
#         if ($anyGO.Success) {
#             $scriptPartLines.RemoveAt($scriptPartLines.Count - 1)
#             $scriptParts.Add($scriptPartLines) > $null
#             $scriptPartLines.Clear()
#         }
#     }

#     return $scriptParts
# }

Clear-Host

$scriptFile = Get-Content .\PreDeploy\DBP-005.sql
#$script = Get-Content .\PreDeploy\DBP-006_NoRunOnceInstruction.sql


$script = New-Object System.Collections.ArrayList($null)
$script.AddRange($scriptFile)

###############################################################################
# Supprimer les lignes vides à la fin du script                               #
###############################################################################

$count = 0

ForEach ($line in $script) {
    $empty = $line.ToString().Trim().Equals("")

    if (-Not $empty) {
        $lastNotEmptyLine = $count
    }

    $count++
}

$script.RemoveRange($lastNotEmptyLine, $script.Count-$lastNotEmptyLine)

###############################################################################
# Séparer le script avec les GO                                               #
###############################################################################

$currentScript = New-Object System.Collections.ArrayList
$scriptParts = New-Object System.Collections.ArrayList

ForEach ($line in $script) {

    $anyGO = [regex]::Match($line, 'GO')
    if (-Not $anyGO.Success) {
        $currentScript.Add($line) > $null
    } else {
        $scriptParts.Add($currentScript) > $null
        $currentScript.Clear()
    }
}

$scriptParts
