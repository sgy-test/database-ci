﻿# == Définition des templates ================================================================

$runOnceCheck = @"

IF NOT EXISTS (SELECT Id FROM dbo.DeploymentScript WHERE Id = @Key)
BEGIN

"@

$runOnceEnd =

@"

END

INSERT INTO dbo.DeploymentScript (Id, Type) VALUES (@key, '@scriptType')

"@

$executeSQLBegin = @"

EXEC sp_executesql '

"@

$executeSQLEnd = @"

'
"@

# == Fonctions ================================================================================

function Split-Script {
    Param(
        [System.Collections.ArrayList] $script
    )

    $currentScript = New-Object -TypeName System.Collections.Generic.List[string]
    $scriptParts = New-Object -TypeName System.Collections.Generic.List[string]

    ForEach ($line in $script) {

        $anyGO = [regex]::Match($line, 'GO')
        if (-Not $anyGO.Success) {
            $currentScript.Add($line) > $null
        } else {
            #$scriptParts.Add($currentScript -join "`n") > $null

            $s = ($currentScript -join "`n").Trim()
            $scriptParts.Add($s) > $null
            $currentScript.Clear()
        }
    }

    # Possible que le script n'ait pas de GO à la fin, si c'est le cas, on dump dans le scriptParts

    if ($currentScript.Count -gt 0) {
        $s = ($currentScript -join "`n").Trim()
        $scriptParts.Add($s) > $null
    }

    return $scriptParts
}

Clear-Host

# ============================================================================================

function Merge-Files {

    Param(
        [object[]] $scriptFiles,
        [string]   $scripType,
        [Boolean]  $runOnce
    )

    Write-Host "Script Type: $scriptType"

    if ($runOnce) {     # On considère que les scripts Data (-Not RunOnce) seront ajoutés au script de PostDeploy.sql
        "DECLARE @Key VARCHAR(255)`n`n"
    }

    ForEach ($scriptFile in $scriptFiles) {

        $filename = $scriptFile.Name

        "-- ScriptFile: $filename -------------------------------------"

        if ($runOnce) {
            $runOnceCheck
        }

        $scriptContent = Get-Content $scriptFile.FullName

        $script = New-Object System.Collections.ArrayList($null)
        $script.AddRange($scriptContent)

        $result = Split-Script $script

        ForEach ($part in $result) {
            $executeSQLBegin

            $part.Replace("'", "''")

            $executeSQLEnd
        }

        if ($runOnce) {
            $runOnceEnd.Replace("@scriptType", $scripType)
        }
    }
}

# == Exécution du script =====================================================================

Clear-Content .\PreDeploy\Script.PreDeployment.sql
$scriptFiles = Get-ChildItem ".\PreDeploy" -Exclude *PreDeployment*
(Merge-Files $scriptFiles "PreDeploy" $true) | Out-File .\PreDeploy\Script.PreDeployment.sql

Clear-Content .\PostDeploy\Script.PostDeployment.sql
$scriptFiles = Get-ChildItem ".\PostDeploy" -Exclude *PostDeployment*
(Merge-Files $scriptFiles "PostDeploy" $true) | Out-File .\PostDeploy\Script.PostDeployment.sql

$scriptFiles = Get-ChildItem ".\Data"
(Merge-Files $scriptFiles "Data" $false) | Add-Content .\PostDeploy\Script.PostDeployment.sql
