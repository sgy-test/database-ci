Write-Host "---------------------------------------------------------------------------------"
Write-Host "Building..."
Write-Host "---------------------------------------------------------------------------------"



MSBuild.exe /target:Clean,Build /p:Configuration=Release ..\DBProjectCI\DBProjectCI.sqlproj



Write-Host "---------------------------------------------------------------------------------"
Write-Host "Packaging..."
Write-Host "---------------------------------------------------------------------------------"


# SqlPackage `
#     /Action:Publish `
#     /SourceFile:"E:\Projects\DBProjectTest\DBProjectCI\DBProjectCI\bin\Release\DBProjectCI.dacpac" `
#     /Profile:"..\DBProjectCI\DBProjectCI.publish.xml" `
#     /TargetServerName:"9YSGY01-PC\SQL2016" `
#     /TargetUser:"sa" `
#     /TargetPassword:"Loft18." `
#     /p:GenerateSmartDefaults=True `
#     /p:BlockOnPossibleDataLoss=false `
#     /p:IncludeTransactionalScripts=True `
#    /p:TreatVerificationErrorsAsWarnings=True
#    /p:VerifyDeployment=False
#     /p:VerifyExtraction=false

SqlPackage `
    /Action:Script `
    /SourceFile:"..\DBProjectCI\bin\Release\DBProjectCI.dacpac" `
    /Profile:"..\DBProjectCI\DBProjectCI.publish.xml" `
    /TargetServerName:"9YSGY01-PC\SQL2016" `
    /TargetDatabaseName:"DBProjectCI_Dev" `
    /p:GenerateSmartDefaults=True `
    /p:BlockOnPossibleDataLoss=false `
    /p:IncludeTransactionalScripts=True `
    /OutputPath:"script.sql"


# SqlPackage `
#     /Action:Publish `
#     /SourceFile:"E:\Projects\DBProjectTest\DBProjectCI\DBProjectCI\bin\Release\DBProjectCI.dacpac" `
#     /Profile:"..\DBProjectCI\DBProjectCI.publish.xml" `
#     /TargetServerName:"9YSGY01-PC\SQL2016" `
#     /TargetDatabaseName:"DBProjectCI_Dev" `
#     /p:GenerateSmartDefaults=True `
#     /p:BlockOnPossibleDataLoss=false `
#     /p:IncludeTransactionalScripts=True
